package io.geier.diplomathesis.experiment.client;

import io.geier.diplomathesis.messaging.MqttPayload;
import org.fusesource.mqtt.client.Callback;
import org.fusesource.mqtt.client.FutureConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class MqttMultiBrokerClient {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMultiBrokerClient.class);

    private final String clientId;
    private final MessageListener messageListener;
    private final List<Broker> brokerList = new ArrayList<>();

    public MqttMultiBrokerClient(String clientId, MessageListener messageListener) {
        this.clientId = clientId;
        this.messageListener = messageListener;
    }

    public MqttMultiBrokerClient(String clientId) {
        this(clientId, null);
    }

    public Future<Broker> connect(String host, int port) {
        CompletableFuture<Broker> connectFuture = new CompletableFuture<>();
        try {
            MQTT mqtt = new MQTT();
            mqtt.setClientId(clientId);
            mqtt.setHost(host, port);

            // make sure, the client does not reconnect, if it gets disconnected by the broker
            mqtt.setReconnectAttemptsMax(0);

            FutureConnection connection = mqtt.futureConnection();
            connection.connect().then(new Callback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Broker broker = new Broker(host, port, connection);
                    synchronized (brokerList) {
                        brokerList.add(broker);
                    }
                    waitForMessage(broker);
                    connectFuture.complete(broker);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    LOG.warn("Could not connect to broker. host={}, port={}, error={}", host, port, throwable.getMessage());
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
            connectFuture.completeExceptionally(e);
        }

        return connectFuture;
    }

    public CompletableFuture<Void> subscribe(String topicName, QoS qos) {
        CompletableFuture<Void> subscribeFuture = new CompletableFuture<>();
        subscribeFuture.complete(null);
        synchronized (brokerList) {
            for (Broker broker : brokerList) {
                Topic[] topics = new Topic[1];
                topics[0] = new Topic(topicName, qos);

                CompletableFuture<Void> newSubscribeFuture = new CompletableFuture<>();
                subscribeFuture.thenCombine(newSubscribeFuture, (aVoid, aVoid2) -> null);
                broker.getConnection().subscribe(topics).then(new Callback<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        LOG.debug("Subscription successful. broker={}, topic={}, qos={}", broker, topicName, qos);
                        newSubscribeFuture.complete(null);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        LOG.warn("Subscription error. broker={}, topic={}, qos={}, error={}", broker, topicName, qos, throwable.getMessage());
                        newSubscribeFuture.completeExceptionally(throwable);
                    }
                });
                subscribeFuture = newSubscribeFuture;
            }
        }
        return subscribeFuture;
    }

    private void waitForMessage(Broker broker) {
        broker.getConnection().receive().then(new Callback<Message>() {
            @Override
            public void onSuccess(Message message) {
                LOG.debug("Message received. broker={}, topic={}", broker, message.getTopic());
                message.ack();
                if (messageListener != null) {
                    messageListener.onMessage(message.getTopic(), MqttPayload.fromBytes(message.getPayload()));
                }
                waitForMessage(broker);
            }

            @Override
            public void onFailure(Throwable throwable) {
                LOG.warn("Could not receive message. broker={}, error={}", broker, throwable.getMessage());
            }
        });
    }

    public CompletableFuture<Void> publish(String topicName, MqttPayload payload, QoS qos) {
        CompletableFuture<Void> publishFuture = new CompletableFuture<>();
        publishFuture.complete(null);
        synchronized (brokerList) {
            for (Broker broker : brokerList) {
                CompletableFuture<Void> newPublishFuture = new CompletableFuture<>();
                publishFuture.thenCombine(newPublishFuture, (aVoid, aVoid2) -> null);
                broker.getConnection().publish(topicName, payload.toBytes(), qos, false).then(new Callback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        LOG.debug("Message published. broker={}", broker);
                        newPublishFuture.complete(null);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        LOG.warn("Could not publish message. broker={}, error={}", broker, throwable.getMessage());
                        newPublishFuture.completeExceptionally(throwable);
                    }
                });
                publishFuture = newPublishFuture;
            }
        }
        return publishFuture;
    }

    public List<Broker> getBrokers() {
        return brokerList;
    }

    public class Broker {

        private final String host;
        private final int port;
        private final FutureConnection connection;

        Broker(String host, int port, FutureConnection connection) {
            this.host = host;
            this.port = port;
            this.connection = connection;
        }

        public String getHost() {
            return host;
        }

        public int getPort() {
            return port;
        }

        FutureConnection getConnection() {
            return connection;
        }

        @Override
        public String toString() {
            return "Broker{" +
                    "host='" + host + '\'' +
                    ", port=" + port +
                    '}';
        }
    }

    public interface MessageListener {
        void onMessage(String topic, MqttPayload payload);
    }
}
