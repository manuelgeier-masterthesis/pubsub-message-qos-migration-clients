package io.geier.diplomathesis.experiment.client;

import io.geier.diplomathesis.experiment.domain.Message;
import io.geier.diplomathesis.experiment.domain.PublishTopic;
import io.geier.diplomathesis.experiment.metric.Metric;
import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.messaging.MqttPayload;
import io.geier.diplomathesis.messaging.MqttPayloadBody;
import io.geier.diplomathesis.messaging.MqttPayloadHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ClientPublisher {

    private static final Logger LOG = LoggerFactory.getLogger(ClientPublisher.class);

    private final String clientId;
    private final String brokerHost;
    private final int brokerPort;
    private final List<PublishTopic> topics;
    private final Metric metric;

    private MqttMultiBrokerClient client;

    public ClientPublisher(String clientId, String brokerHost, int brokerPort, List<PublishTopic> topics) {
        this(clientId, brokerHost, brokerPort, topics, new Metric());
    }

    public ClientPublisher(String clientId, String brokerHost, int brokerPort, List<PublishTopic> topics, Metric metric) {
        this.clientId = clientId;
        this.brokerHost = brokerHost;
        this.brokerPort = brokerPort;
        this.topics = topics;
        this.metric = metric;

        client = new MqttMultiBrokerClient(clientId);
    }

    public String getClientId() {
        return clientId;
    }

    public ClientPublisher connect() {
        LOG.info("Connect to " + brokerHost + ":" + brokerPort + " ...");
        try {
            client.connect(brokerHost, brokerPort).get();
            LOG.info("Connected!");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return this;
    }

    public void start() {
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(topics.size());
        for (PublishTopic publishTopic : topics) {
            scheduler.scheduleAtFixedRate(() -> {
                try {
                    publishTopic.messageNr++;

                    Message message = new Message("" + publishTopic.messageNr, brokerHost, "" + brokerPort, publishTopic.name);
                    LOG.info("Message generated. topic={}, message={}", publishTopic.name, message);

                    GlobalMessageId globalMessageId = new GlobalMessageId(publishTopic.name, "" + publishTopic.messageNr);
                    MqttPayload payload = new MqttPayload(new MqttPayloadHeader(globalMessageId), new MqttPayloadBody(message.toString()));

                    logMessage(clientId, publishTopic.name, globalMessageId);
                    client.publish(publishTopic.name, payload, publishTopic.qos).thenRun(() ->
                            LOG.info("Message published. topic={}, message={}", publishTopic.name, message));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, publishTopic.delay, publishTopic.interval, TimeUnit.MILLISECONDS);
        }
    }

    public void logMessage(String clientId, String topicName, GlobalMessageId globalMessageId) {
        metric.track(clientId, topicName, globalMessageId, brokerHost + ":" + brokerPort, Metric.Type.PRODUCED);
    }
}
