package io.geier.diplomathesis.experiment.domain;

import org.fusesource.mqtt.client.QoS;

public class TopicNew {

    public String name;
    public QoS qos;

    public TopicNew(String name, QoS qos) {
        this.name = name;
        this.qos = qos;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "name='" + name + '\'' +
                ", qos=" + qos +
                '}';
    }
}
