package io.geier.diplomathesis.experiment.main;

import io.geier.diplomathesis.experiment.client.ClientPublisher;
import io.geier.diplomathesis.experiment.domain.PublishTopic;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PublisherClient {

    private static final Logger LOG = LoggerFactory.getLogger(PublisherClient.class);

    public static void main(String[] args) throws IOException {
        String propFileName = args[0];
        try (FileInputStream inStream = new FileInputStream(propFileName)) {
            Properties prop = new Properties();
            prop.load(inStream);
            new PublisherClient(prop);
        }
    }

    public PublisherClient(Properties config) {
        String clientId = config.getProperty("id");
        String brokerHost = config.getProperty("broker.host");
        int brokerPort = Integer.parseInt(config.getProperty("broker.port"));
        String topics = config.getProperty("topics");

        // topicname:interval:delay,topicname:interval:delay,...
        // format: topic1:500:0,topic2:1000:250,...
        List<PublishTopic> publishTopics = new ArrayList<>();
        for (String topicDesc : topics.split(",")) {
            String[] split = topicDesc.split(":");
            String topicName = split[0];
            int qos = Integer.parseInt(split[1]);
            int interval = Integer.parseInt(split[2]);
            int delay = Integer.parseInt(split[3]);
            publishTopics.add(new PublishTopic(topicName, toQoS(qos), interval, delay));
        }

        ClientPublisher client = new ClientPublisher(clientId, brokerHost, brokerPort, publishTopics);
        client.connect();
        client.start();
    }

    private QoS toQoS(int qos) {
        switch (qos) {
            case 0:
                return QoS.AT_MOST_ONCE;
            case 1:
                return QoS.AT_LEAST_ONCE;
            case 2:
                return QoS.EXACTLY_ONCE;
            default:
                return null;
        }
    }
}
