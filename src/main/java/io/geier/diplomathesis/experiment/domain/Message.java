package io.geier.diplomathesis.experiment.domain;

import java.util.HashMap;

public class Message {

    public static final String NR = "nr";
    public static final String BROKER_HOST = "broker_host";
    public static final String BROKER_PORT = "broker_port";
    public static final String TOPIC = "topic";

    public HashMap<String, String> props = new HashMap<>();

    public Message() {
    }

    public Message(String messageNr, String serverHost, String serverPort, String topicName) {
        props.put(NR, messageNr);
        props.put(BROKER_HOST, serverHost);
        props.put(BROKER_PORT, serverPort);
        props.put(TOPIC, topicName);
    }

    public static Message fromString(String string) {
        Message message = new Message();
        for (String s : string.split(",")) {
            String[] split = s.split("=");
            message.props.put(split[0], split[1]);
        }
        return message;
    }

    @Override
    public String toString() {
        return getPair(NR) + "," + getPair(BROKER_HOST) + "," + getPair(BROKER_PORT) + "," + getPair(TOPIC);
    }

    private String getPair(String key) {
        return key + "=" + props.get(key);
    }
}
