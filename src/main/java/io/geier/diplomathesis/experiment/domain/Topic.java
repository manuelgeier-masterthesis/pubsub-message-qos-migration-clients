package io.geier.diplomathesis.experiment.domain;

import io.netty.handler.codec.mqtt.MqttQoS;

public class Topic {
    public String name;
    public MqttQoS qos;

    public Topic(String name, MqttQoS qos) {
        this.name = name;
        this.qos = qos;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "name='" + name + '\'' +
                ", qos=" + qos +
                '}';
    }
}
