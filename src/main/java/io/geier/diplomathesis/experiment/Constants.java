package io.geier.diplomathesis.experiment;

public class Constants {

    public static final String TOPIC_TOPIC1 = "topic1";
    public static final String TOPIC_TOPIC2 = "topic2";
    public static final String TOPIC_TOPIC3 = "topic3";
    public static final String TOPIC_TOPIC4 = "topic4";
    public static final String TOPIC_TOPIC5 = "topic5";
    public static final String TOPIC_TOPIC6 = "topic6";
}
