package io.geier.diplomathesis.experiment.domain;

import org.fusesource.mqtt.client.QoS;

public class PublishTopic {

    public String name;
    public final QoS qos;
    public final int interval;
    public final int delay;
    public int messageNr = 0;

    public PublishTopic(String topicName, QoS qos, int interval, int delay) {
        this.name = topicName;
        this.qos = qos;
        this.interval = interval;
        this.delay = delay;
    }

    @Override
    public String toString() {
        return "PublishTopic{" +
                "name='" + name + '\'' +
                ", interval=" + interval +
                ", delay=" + delay +
                '}';
    }
}
