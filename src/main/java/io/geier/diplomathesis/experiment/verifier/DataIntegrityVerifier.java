package io.geier.diplomathesis.experiment.verifier;

import io.geier.diplomathesis.experiment.domain.TopicNew;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DataIntegrityVerifier {

    private static final Logger LOG = LoggerFactory.getLogger(DataIntegrityVerifier.class);

    // this value should be higher than the production rate
    private static final int MESSAGE_TIMEOUT_MS = 4000;

    private final String clientId;

    private Map<String, TopicDataIntegrityValidator> topicDataIntegrityCheckers = new HashMap<>();

    public DataIntegrityVerifier(String clientId) {
        this.clientId = clientId;
    }

    public void registerTopic(TopicNew topic) {
        TopicDataIntegrityValidator topicDataIntegrityValidator = new TopicDataIntegrityValidator(topic);
        topicDataIntegrityCheckers.put(topic.name, topicDataIntegrityValidator);
    }

    public void messageReceived(String topicName, String messageNr) {
        topicDataIntegrityCheckers.get(topicName).messageReceived(Long.parseLong(messageNr));
    }

    class TopicDataIntegrityValidator {

        TopicNew topic;

        Map<Long, Message> messages = new HashMap<>();
        List<Long> messageNrs = new ArrayList<>();
        List<Message> duplicateMessages = new ArrayList<>();
        ScheduledExecutorService scheduler;
        Long lastMessageReceivedAt;

        public TopicDataIntegrityValidator(TopicNew topic) {
            this.topic = topic;
            startMessageTimeoutChecker();
        }

        synchronized public void messageReceived(Long messageNr) {
            lastMessageReceivedAt = System.currentTimeMillis();
            boolean ok = true;
            if (topic.qos == QoS.AT_LEAST_ONCE || topic.qos == QoS.EXACTLY_ONCE) {
                ok &= processMissingMessages(messageNr);
            }
            if (topic.qos == QoS.AT_MOST_ONCE || topic.qos == QoS.EXACTLY_ONCE) {
                ok &= processDuplicateMessages(messageNr);
            }
            if (ok) {
                LOG.info("Ok. clientId={}, topic={}, qos={}", clientId, topic.name, topic.qos);
            }
        }

        private void startMessageTimeoutChecker() {
            if (scheduler == null) {
                scheduler = Executors.newScheduledThreadPool(1);
                scheduler.scheduleAtFixedRate(() -> {
                    long currentTime = System.currentTimeMillis();
                    if (lastMessageReceivedAt == null || (currentTime - MESSAGE_TIMEOUT_MS > lastMessageReceivedAt)) {
                        LOG.warn("Waiting for messages. clientId={}, topic={}, qos={}", clientId, topic.name, topic.qos);
                    }
                }, MESSAGE_TIMEOUT_MS, MESSAGE_TIMEOUT_MS, TimeUnit.MILLISECONDS);
            }
        }

        private boolean processMissingMessages(Long messageNr) {
            messageNrs.add(messageNr);

            if (messageNrs.size() > 1) {
                Collections.sort(messageNrs);

                Long firstNr = messageNrs.get(0);
                Long lastNr = messageNrs.get(messageNrs.size() - 1);

                List<Long> missingNrs = new ArrayList<>();

                Long nextNr = firstNr;
                while (nextNr <= lastNr) {
                    if (!messageNrs.contains(nextNr)) {
                        missingNrs.add(nextNr);
                    }
                    nextNr++;
                }

                if (!missingNrs.isEmpty()) {
                    LOG.error("Message inconsistency. clientId={}, topic={}, qos={}, missingNrs={}", clientId, topic.name, topic.qos, missingNrs);
                    return false;
                }
            }
            return true;
        }

        private boolean processDuplicateMessages(Long messageNr) {
            Message message = messages.computeIfAbsent(messageNr, msgNr -> new Message(msgNr, 0));
            message.count++;
            if (message.count == 2) {
                duplicateMessages.add(message);
            }

            if (!duplicateMessages.isEmpty()) {
                LOG.error("Message duplication. clientId={}, topic={}, qos={}, duplicateMessages={}", clientId, topic.name, topic.qos, duplicateMessages);
                return false;
            }

            return true;
        }

    }

    class Message {
        long nr;
        int count;

        public Message(long nr, int count) {
            this.nr = nr;
            this.count = count;
        }

        @Override
        public String toString() {
            return nr + ": " + count;
        }

    }
}
