package io.geier.diplomathesis.experiment.main;

import io.geier.diplomathesis.experiment.client.ClientSubscriber;
import io.geier.diplomathesis.experiment.domain.TopicNew;
import io.geier.diplomathesis.experiment.metric.Metric;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SubscriberClient {

    private static final Logger LOG = LoggerFactory.getLogger(SubscriberClient.class);

    public static void main(String[] args) throws IOException {
        String propFileName = args[0];
        try (FileInputStream inStream = new FileInputStream(propFileName)) {
            Properties prop = new Properties();
            prop.load(inStream);
            new SubscriberClient(prop);
        }
    }

    public SubscriberClient(Properties config) {
        String clientId = config.getProperty("id");
        String brokerHost = config.getProperty("broker.host");
        int brokerPort = Integer.parseInt(config.getProperty("broker.port"));
        int cliPort = Integer.parseInt(config.getProperty("cli.port"));
        int migPort = Integer.parseInt(config.getProperty("migration.port"));
        String subscriptions = config.getProperty("subscriptions");

        // format: topic1:0,topic2:1,topic3:2
        List<TopicNew> topics = new ArrayList<>();
        for (String subscription : subscriptions.split(",")) {
            String[] split = subscription.split(":");
            String topicName = split[0];
            int qos = Integer.parseInt(split[1]);
            topics.add(new TopicNew(topicName, toQoS(qos)));
        }

        new ClientSubscriber(clientId, brokerHost, brokerPort, topics, cliPort, migPort, new Metric())
                .connect()
                .start();
    }

    private QoS toQoS(int qos) {
        switch (qos) {
            case 0:
                return QoS.AT_MOST_ONCE;
            case 1:
                return QoS.AT_LEAST_ONCE;
            case 2:
                return QoS.EXACTLY_ONCE;
            default:
                return null;
        }
    }

}
