package io.geier.diplomathesis.experiment.metric;

import io.geier.diplomathesis.messaging.GlobalMessageId;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Metric {

    public enum Type {
        PRODUCED, RECEIVED
    }

    File outputFolder = new File("/shared/output");
    File outputFile;
    OutputStreamWriter writer;

    public Metric() {
        outputFolder.mkdirs();
        outputFile = new File(outputFolder, "metrics.csv");

        try {
            writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outputFile)));
            writer.write(String.format("clientId,topic,guid,broker,timestamp,type%n"));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void track(String clientId, String topicName, GlobalMessageId globalMessageId, String broker, Type type) {
        long timestamp = System.currentTimeMillis();
        try {
            writer.write(String.format("%s,%s,%s,%s,%d,%s%n", clientId, topicName, globalMessageId.toString(), broker, timestamp, type));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
