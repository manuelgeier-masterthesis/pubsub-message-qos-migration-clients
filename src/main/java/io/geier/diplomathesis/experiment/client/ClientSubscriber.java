package io.geier.diplomathesis.experiment.client;

import io.geier.diplomathesis.experiment.Constants;
import io.geier.diplomathesis.experiment.domain.Message;
import io.geier.diplomathesis.experiment.domain.TopicNew;
import io.geier.diplomathesis.experiment.metric.Metric;
import io.geier.diplomathesis.experiment.verifier.DataIntegrityVerifier;
import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.messaging.MqttPayload;
import io.geier.diplomathesis.migration.client.ClientMigrationManager;
import io.geier.diplomathesis.migration.client.MigrationClientBridge;
import io.geier.diplomathesis.migration.client.cli.ClientCLI;
import io.geier.diplomathesis.migration.client.cli.domain.BrokerInfo;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class ClientSubscriber implements MqttMultiBrokerClient.MessageListener {

    private static final Logger LOG = LoggerFactory.getLogger(ClientSubscriber.class);

    private final String clientId;
    private final String serverHost;
    private final int serverPort;
    private final List<TopicNew> topics;
    private final int messageServerPort;
    private final Map<String, TopicNew> topicsMap = new HashMap<>();

    private final MqttMultiBrokerClient client;
    private final DataIntegrityVerifier dataIntegrityValidator;
    private final Metric metric;

    public static void main(String[] args) {
        List<TopicNew> topics = new ArrayList<>();
        topics.add(new TopicNew(Constants.TOPIC_TOPIC1, QoS.AT_MOST_ONCE));
//        topics.add(new Topic(Constants.TOPIC_TOPIC2, QoS.AT_LEAST_ONCE));
//        topics.add(new Topic(Constants.TOPIC_TOPIC3, QoS.EXACTLY_ONCE));
//        topics.add(new Topic(Constants.TOPIC_TOPIC4, QoS.AT_MOST_ONCE));
//        topics.add(new Topic(Constants.TOPIC_TOPIC5, QoS.AT_LEAST_ONCE));
//        topics.add(new Topic(Constants.TOPIC_TOPIC6, QoS.EXACTLY_ONCE));
        new ClientSubscriber(null, "localhost", 1884, topics, 1701, 1702, new Metric()).connect().start();
    }

    public ClientSubscriber(final String clientId, final String serverHost, final int serverPort, List<TopicNew> topics, final int consolePort, final int messageServerPort, Metric metric) {
        this.clientId = clientId;
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.topics = topics;
        this.messageServerPort = messageServerPort;
        this.metric = metric;

        for (TopicNew topic : topics) {
            this.topicsMap.put(topic.name, topic);
        }

        client = new MqttMultiBrokerClient(clientId, this);

        MigrationClientBridge clientBridge = new MigrationClientBridge() {
            @Override
            public Future connect(String host, int port) {
                return client.connect(host, port);
            }

            @Override
            public List<BrokerInfo> getBrokers() {
                return client.getBrokers().stream().map(broker -> new BrokerInfo(broker.getHost(), broker.getPort())).collect(Collectors.toList());
            }
        };

        if (messageServerPort != 0) {
            new ClientMigrationManager(messageServerPort, clientBridge);
        }

        if (consolePort != 0) {
            new ClientCLI(consolePort, clientBridge).start();
        }

        dataIntegrityValidator = new DataIntegrityVerifier(clientId);
        LOG.info("Client created. clientId={}", clientId);

        //copyClientIdToClipboard(client.getClientConfig().getClientId());
    }

    private void copyClientIdToClipboard(String clientId) {
        Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .setContents(
                        new StringSelection(clientId),
                        null
                );
        LOG.info("Client id copied into clipboard.");
    }

    public ClientSubscriber connect() {
        try {
            LOG.info("Connect to {}:{} ...", serverHost, serverPort);
            client.connect(serverHost, serverPort).get();
            LOG.info("Connected!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void start() {
        try {
            LOG.info("Subscribe to topics ...");
            for (TopicNew topic : topics) {
                registerTopic(topic);
            }
            LOG.info("Subscribed to topic!");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public String getClientId() {
        return clientId;
    }

    public int getMessageServerPort() {
        return messageServerPort;
    }

    private void registerTopic(TopicNew topic) throws ExecutionException, InterruptedException {
        LOG.info("Register topic. topic={}", topic);

        dataIntegrityValidator.registerTopic(topic);
        client.subscribe(topic.name, topic.qos).get();
    }

    @Override
    public void onMessage(String topic, MqttPayload payload) {
        String messageString = payload.getBody().getPayload();
        Message message = Message.fromString(messageString);
        String messageNr = message.props.get(Message.NR);
        String senderHost = message.props.get(Message.BROKER_HOST);
        String senderPort = message.props.get(Message.BROKER_PORT);
        GlobalMessageId globalMessageId = new GlobalMessageId(topic, "" + messageNr);
        LOG.info("Message received. clientId={}, topic={}, qos={}, globalMessageId={}, messageNr={}, message={}", clientId, topic, getQoS(topic), globalMessageId, messageNr, messageString);

        metric.track(clientId, topic, globalMessageId, senderHost + ":" + senderPort, Metric.Type.RECEIVED);
        dataIntegrityValidator.messageReceived(topic, messageNr);
    }

    private QoS getQoS(String topic) {
        return topicsMap.get(topic).qos;
    }
}
