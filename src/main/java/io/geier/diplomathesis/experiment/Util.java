package io.geier.diplomathesis.experiment;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.charset.Charset;

public class Util {

    public static ByteBuf toByteBuffer(String string) {
        return Unpooled.wrappedBuffer(string.getBytes());
    }

    public static String fromByteBuffer(ByteBuf byteBuf) {
        return byteBuf.toString(Charset.defaultCharset());
    }

}
